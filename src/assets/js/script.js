$(function() {
    /*
        === GLOBAL ===
    */

    // Object-fit fallback
    if('objectFit' in document.documentElement.style === false) {
        var container = document.getElementsByClassName('cover-container');
        for(var i = 0; i < container.length; i++) {
            var imageSource = container[i].querySelector('img').src;
            container[i].querySelector('img.cover').style.display = 'none';
            container[i].style.backgroundImage    = 'url(' + imageSource + ')';
            container[i].style.backgroundSize     = 'cover';
            container[i].style.backgroundPosition = 'center';
            container[i].style.backgroundRepeat   = 'no-repeat';
        }
    }
});

$(window).on('load', function() {
});